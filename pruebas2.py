""" Pruebas con cadenas """

import unittest

class PruebasMetodosCadenas(unittest.TestCase):
    
    def test_upper(self):
        self.assertEqual('misiontic'.upper(), 'MISIONTIC')
        
    def test_isupper(self):
        self.assertTrue('misiontic'.isupper())
        self.assertFalse('misiontic'.isupper())
        
    def test_split(self):
        s = "Mision Tic"
        self.assertEqual(s.split(), ['Mision', 'Tic', '2021'])
        
if __name__ == '__main__':
    unittest.main()